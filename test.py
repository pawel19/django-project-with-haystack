#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, django, glob, sys, shelve
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysite.settings")

django.setup()

import json, requests
from polls.models import Question


data = ''
for p in Question.objects.all():
	data += '{"index": {"_id": "%s"}}\n' % p.pk
	data += json.dumps({
		"question_text": p.question_text,
		"pub_date": str(p.pub_date)
	})+'\n'
response = requests.put('http://127.0.0.1:9200/my_index/polls/_bulk', data=data)
print response.text

print

data = {
    "query": {
        "query_string": {
           "default_field": "question_text",
           "query": "your"
        }
    }
}
response = requests.post('http://127.0.0.1:9200/my_index/polls/_search', data=json.dumps(data))
print response.json()
