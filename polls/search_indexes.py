from django.utils import timezone
from haystack import indexes
import datetime

from .models import Question


class QuestionIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    '''
    question_text = indexes.CharField(model_attr='question_text')

    def prepare_question_text(self, object):
        return object.question_text.lower()
    
    #pub_date = indexes.DateTimeField(model_attr='pub_date')
    #Only one field# text = indexes.CharField(document=True, model_attr='question_text')
    #content_auto = indexes.EdgeNgramField(model_attr='content')
    '''

    def get_model(self):
        return Question

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(pub_date__lte=timezone.now())
