#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, django, glob, sys, shelve

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysite.settings")
django.setup()

from haystack.query import SearchQuerySet

from polls.models import Question

results=SearchQuerySet().models(Question)#.filter(content='markdown')

load_results=SearchQuerySet().models(Question).load_all()#filter(content='markdown').load_all()

print load_results
